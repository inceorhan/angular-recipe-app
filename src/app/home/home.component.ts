import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/Rx';
import { RecipeService } from '../recipe.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  recipes: any[];
  id: number;
  constructor(private recipeService: RecipeService,
  private router: Router,
  private route: ActivatedRoute,) { }

  ngOnInit() {
    this.getAllRecipes();
  }

  getAllRecipes() {
    this.recipeService.getRecipes()
    .subscribe(
      (recipes: any[]) => this.recipes = recipes
    )
  }

  deleteRecipe(id) {
    if(confirm("Are you sure?")) {
      return this.recipeService.deleteRecipe(id).toPromise()
      .then(() => {
        this.getAllRecipes();
        this.router.navigate(['']);
      })
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Params, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, Validator, Validators } from '@angular/forms';
import { RecipeService } from '../recipe.service';
import { Recipe } from '../recipe.model';
import { Ingredient } from '../ingredient.model';
import {Location} from '@angular/common';

@Component({
  selector: 'app-recipe-update',
  templateUrl: './recipe-update.component.html',
  styleUrls: ['./recipe-update.component.css']
})
export class RecipeUpdateComponent implements OnInit {
  id:number;
  Id:any = null;
  editMode = false;
  recipeForm: FormGroup;
  recipeData;
  recipeName = '';
  recipePrepTime = '';
  recipeCookingTime = '';
  recipeId;
  recipeIngredients = new FormArray([]);
  ingredients = [];
  newIngredientsArray = [];
  recipes = [];
  message: string = '';
  isOkay: boolean = false;
  constructor(private route: ActivatedRoute,
    private recipeService: RecipeService,
    private router:Router,
    private location: Location) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.editMode = params['id'] != null;
          this.initForm();
          if(this.editMode) {
            this.getData();
          }
        }
      )
  }

  // create initForm > onInit
  private initForm() {
       if(this.ingredients) {
        for (let ingredient of this.ingredients) {
          this.generateFormGroup(this.recipeIngredients,ingredient);
        }
       }
       this.createForm();
  }

  // generate FormGroup func
  private generateFormGroup(formGroupArray,value = null) {
    formGroupArray.push(
      new FormGroup({
        'Id': new FormControl(value.Id),
        'Name': new FormControl(value.Name),
        'Quantity': new FormControl(value.Quantity)
      })
    )
  }

  // getData from the server
  private getData() {

    this.recipeService.getRecipe(this.id)
    .subscribe(
      result => {
        this.recipeData = result;
        this.getIngredients();
        this.recipeName = this.recipeData.Name;
        this.recipeId = this.recipeData.Id;
        this.recipePrepTime = this.recipeData.PrepTime;
        this.recipeCookingTime = this.recipeData.CookingTime;
        this.createForm();
      }
    )      
  }

  // generate Ingredient Form > form controls
  private creaetIngredientForm() {
    if(this.ingredients) {
      for (let ingredient of this.ingredients) {
        this.generateFormGroup(this.recipeIngredients,ingredient)
      }
    }
  }

  // create general form
  private createForm() {
    this.recipeForm = new FormGroup({
      Name: new FormControl(this.recipeName, Validators.required),
      PrepTime : new FormControl(this.recipePrepTime, Validators.required),
      CookingTime: new FormControl(this.recipeCookingTime, Validators.required),
      Ingredients: this.recipeIngredients
    })
  }

  // get ingredients data from the server
  private getIngredients() {
    this.recipeService.getIngredients(this.id)
    .subscribe(
      result => {
        this.ingredients = result;
        this.creaetIngredientForm();
      }
    )
  }

  // add new ingredient from group
  private addIngredient() {
    (<FormArray>this.recipeForm.get('Ingredients')).push(
      new FormGroup({
        'Id': new FormControl(null),
        'Name': new FormControl(null, Validators.required),
        'Quantity': new FormControl(null, Validators.required)
      })
    )
  }

  // delete ingredient from from group and the server
  onDeleteIngredient(index: number) {
    for(var key in this.recipeForm.value['Ingredients']) {
      let deletingItem = this.recipeForm.value['Ingredients'][index];
      this.recipeService.deleteIngredient(this.id,deletingItem['Id'])
      .subscribe(
        result => {
          console.log(result);
        }
      )
    }
    (<FormArray>this.recipeForm.get('Ingredients')).removeAt(index);
  }

  // add ingredient item to the server
  private addIngredientItem(recipeId,index) {
    const newIngredient = new Ingredient(
      this.newIngredientsArray[index]['Name'],
      this.newIngredientsArray[index]['Quantity']
    )
    this.recipeService.addIngredient(recipeId,newIngredient)
    .subscribe(
      result => {
        //console.log(result);
      }
    )
  }

  // update ingredient from the server
  private updateIngredientItem(index) {
    let iId =  this.newIngredientsArray[index]['Id'];
        const newIngredient = new Ingredient(
          this.newIngredientsArray[index]['Name'],
          this.newIngredientsArray[index]['Quantity']
        )
        this.recipeService.updateIngredient(this.id,iId, newIngredient)
        .subscribe(
          result => {
            //console.log(result);
          }
        )
  }

  // submit the recipeForm
  onSubmit() {
    const newRecipe = new Recipe(
      this.recipeForm.value['Name'],
      this.recipeForm.value['PrepTime'],
      this.recipeForm.value['CookingTime']
    );
    if(this.editMode) {




      this.recipeService.updateRecipe(this.id, newRecipe)
      .subscribe(
        result => {
          const newIngredients = this.recipeForm.value['Ingredients'];
          this.newIngredientsArray = newIngredients
      for(let i= 0; i < this.newIngredientsArray.length; i++) {
        if(this.newIngredientsArray[i]['Id'] == null) {
          this.addIngredientItem(this.id,i);
        }else {
          this.updateIngredientItem(i);
        }
        
      }
      this.getAllRecipes();
      this.isOkay = true;
      this.message = "The recipe has been updated!";
        }
      );
    }else {
      let newResultId:number;
      this.recipeService.addRecipe(newRecipe)
      .subscribe(
        result => {
          console.log(result.json())
          let newResult = result.json();
          newResultId = newResult.Id;
          console.log(newResultId + ' ahanda id budur')
      const newIngredients = this.recipeForm.value['Ingredients'];
      this.newIngredientsArray = newIngredients
      for(let i= 0; i < this.newIngredientsArray.length; i++) {
        if(this.newIngredientsArray[i]['Id'] == null) {
          this.addIngredientItem(newResultId,i);
        }
      }
      this.isOkay = true;
      this.message = "The recipe has been added!";
      console.log(this.message)
        }
      );
    }
  }

  getAllRecipes() {
    this.recipeService.getRecipes()
    .subscribe(
      (recipes: any[]) => this.recipes = recipes
    )
  }

  returnBack() {
    this.location.back();
  }

  
}

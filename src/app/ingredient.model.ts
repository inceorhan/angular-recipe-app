export class Ingredient {
    constructor(
        public Name:string, 
        public Quantity:string
    ){
    }
}
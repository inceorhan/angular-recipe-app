import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AppRouteModule } from './app-route.module';
import { RecipeService } from './recipe.service';
import { RecipeUpdateComponent } from './recipe-update/recipe-update.component';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RecipeUpdateComponent,
    RecipeDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRouteModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule 
  ],
  providers: [RecipeService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { Route, ActivatedRoute, Router, Params } from '@angular/router';
import { RecipeService } from '../recipe.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  
  id: number;
  recipe = {
    Id: null,
  };
  ingredients: any[];
  recipes: any[];
  recipeIngredients: any;
  constructor(private route: ActivatedRoute,
  private router: Router,
  private recipeService: RecipeService,
  private location: Location) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
        }
      )
      this.recipeService.getRecipe(this.id)
      .subscribe(
        result => {
          this.recipe = result;
        }
      )

      this.recipeService.getIngredients(this.id)
      .subscribe(
        result => {
          this.ingredients = result;
        }
      )

      this.getAllRecipes();
  }
  getAllRecipes() {
    this.recipeService.getRecipes()
    .subscribe(
      (recipes: any[]) => this.recipes = recipes
    )
  }

  getRecipeIngredients(id) {
    this.recipeService.getRecipeIngredients(id)
    .subscribe(
      result => {
        this.recipeIngredients = result;  
      }
    )
  }

  deleteRecipe(id) {
    if(confirm("Are you sure?")) {
      return this.recipeService.deleteRecipe(id).toPromise()
      .then(() => {
        for(var key in this.recipeIngredients) {
          this.recipeService.deleteIngredient(this.id,this.recipeIngredients[key]['Id'])
          .subscribe(
            result => {
            }
          )  
        }
        this.getAllRecipes();
        this.router.navigate(['']);
      })
    }
  }

  returnBack() {
    this.location.back();
  }

}

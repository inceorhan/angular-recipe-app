export class Recipe {
    constructor(
        public Name:string, 
        public PrepTime:string, 
        public CookingTime: string
    ){
    }
}
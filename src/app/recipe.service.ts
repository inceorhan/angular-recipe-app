import { Injectable } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/Rx';
import { Recipe } from './recipe.model';
import { Ingredient } from './ingredient.model';
@Injectable()

export class RecipeService {
    
    constructor(private http:Http) {
       
    }

    storeRecipes(recipes: any[]){
        return this.http.post('http://api.10goto20.com/v1/recipe/murat2017','')
    }

    getRecipes() {
        return this.http.get('http://api.10goto20.com/v1/recipe/murat2017')
        .map(
            (response: Response) => {
                const data = response.json();
                return data;
            }
        )
    }

    getRecipe(id: number) {
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.get('http://api.10goto20.com/v1/recipe/murat2017/' + id,{headers: headers})
        .map(
            (response: Response) => {
                const data = response.json();
                return data;
                
            }
        )
        
    }

    getRecipeIngredient(id: number) {
        
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.get('http://api.10goto20.com/v1/recipe/murat2017/' + id + '/ingredient',{headers: headers})
        .map(
            (response: Response) => {
                const data = response.json();
                return data;
            }
        )
    }

    addRecipe(recipe: Recipe) {
        let headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post('http://api.10goto20.com/v1/recipe/murat2017', recipe,options)
    }

    updateRecipe(id: number, recipe: Recipe) {
        let headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
        let options = new RequestOptions({ headers: headers });
        return this.http.put('http://api.10goto20.com/v1/recipe/murat2017/' + id, recipe, options)
    }

    deleteRecipe(id: number) {
        return this.http.delete(' http://api.10goto20.com/v1/recipe/murat2017/'+id)
    }

    getIngredients(id: number) {
        return this.http.get('http://api.10goto20.com/v1/recipe/murat2017/' + id + '/ingredient')
        .map(
            (response: Response) => {
                const data = response.json();
                return data;
            }
        )
    }

    updateIngredient(rId: number,iId:number,ingredient:Ingredient) {
        let headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
        let options = new RequestOptions({ headers: headers });
        return this.http.put('http://api.10goto20.com/v1/recipe/murat2017/' + rId + '/ingredient/' + iId, ingredient, options)
    }

    addIngredient(rId: number, ingredient: Ingredient) {
        let headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(' http://api.10goto20.com/v1/recipe/murat2017/' + rId + '/ingredient', ingredient,options)
       
    }

    deleteIngredient(rId: number,iId: number) {
        return this.http.delete('http://api.10goto20.com/v1/recipe/murat2017/' + rId +'/ingredient/' + iId); 
    }

    getRecipeIngredients(id: number) {
        
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.get('http://api.10goto20.com/v1/recipe/murat2017/' + id + '/ingredient',{headers: headers})
        .map(
            (response: Response) => {
                const data = response.json();
                return data;
            }
        )
    }
    
}
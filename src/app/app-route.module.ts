import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RecipeUpdateComponent } from './recipe-update/recipe-update.component';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent},
    { path: 'recipe/new', component: RecipeUpdateComponent},
    { path: 'recipe/:id', component: RecipeDetailComponent},
    { path: 'recipe/edit/:id', component: RecipeUpdateComponent}
]

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})

export class AppRouteModule {

}